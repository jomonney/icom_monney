<?php require_once 'inc/config.php' ;?>
  <!doctype html>
  <html class="no-js" lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>My task</title>
      <link rel="stylesheet" href="css/app.css">
      <link rel="stylesheet" href="scss/modules/font-awesome-4.7.0/css/font-awesome.css">
    </head>

  <body>
  <?php require_once 'header.php' ;?>
        <div class="container-login">
          <div class="hide-for-small-only">
        <img class="image-login" src="etc/img/login.png">
      </div>
          <form method="post" action="logme.php">
          <label for="date">User</label>
          <input name="name"type="text"/></p>

          <label for="date">Password</label>
          <input name="password"type="password"/></p>

          <input type="submit" value="login">
        </div>
      <?php require_once 'footer.php' ;?>
    </div>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
  </html>
