<?php require_once 'inc/config.php' ;?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Task</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="scss/modules/font-awesome-4.7.0/css/font-awesome.css">
  </head>
  <body>
      <?php require_once 'header.php' ;?>

      <div class="container">
        <div class="month">
          <ul class="month-list-titre">
              <li class="tasklist-item-titre">
              <span class="tasklist-item-id">#</span>

              <span class="tasklist-item-moi1">janvier</span>
              <span class="tasklist-item-moi2">fevrier</span>
              <span class="tasklist-item-moi3">mars</span>
              <span class="tasklist-item-moi4">avril</span>
              <span class="tasklist-item-moi5">mai</span>
              <span class="tasklist-item-moi6">juin</span>
              <span class="tasklist-item-moi7">juillet</span>
              <span class="tasklist-item-moi8">aout</span>
              <span class="tasklist-item-moi9">septembre</span>
              <span class="tasklist-item-moi9">octobre</span>
              <span class="tasklist-item-moi9">novembre</span>
              <span class="tasklist-item-moi9">decembre</span>
          </div>
          </li>
        </ul>
        <?php foreach ($data as $row):?>
        <ul class="monthlist">
          <li>
            <span class="tasklist-item-id"><?php echo $row['id']?></span>
            <span class="tasklist-item-id"><?php echo $row['name']?></span>
            <span class="tasklist-item-id"><?php echo $row['start']?></span>
            <span class="tasklist-item-id"><?php echo $row['end']?></span>
            <a href="delete.php?task=<?php echo $row['id'];?>" class="fa fa-window-close">Delete</a></li>
      </ul>
       <?php endforeach?>
       <div class="ajout">
       <a href="edit.php"><p>ajouter une tache</p></a>
       <a href="logout.php"><p>logout</p></a>
     </div>
     </div>

</div>
</div>
            <script src="bower_components/jquery/dist/jquery.js"></script>
            <script src="bower_components/what-input/dist/what-input.js"></script>
            <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
            <script src="js/app.js"></script>
  </div>
</div>
</body>
</html>
